import React, {useEffect, useState} from 'react';

function SalesPersonHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesPerson] = useState('');
    const [filteredSales, setFilteredSales]= useState([]);

    const handleSalespersonchange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }
    const fetchSalesData = async () => {
        const salesUrl = `http://localhost:8090/api/sales/`;
        const salesResponse = await fetch(salesUrl);
        if(salesResponse.ok){
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        }
    }
    const fetchSalespeopleData = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespeopleUrl);
        if(response.ok){
            const salespeopleData = await response.json();
            setSalespeople(salespeopleData.salesperson)
        }
    }


    useEffect(()=>{
        fetchSalesData();
        fetchSalespeopleData();
    }, []);

    useEffect(() => {
        const filtered = salesperson
            ? sales.filter(sale => sale.salesperson.employee_id === salesperson)
            : sales ;
        setFilteredSales(filtered);
    }, [sales, salesperson]);

    return (
        <div>
            <h1>Salesperson History</h1>
            <select onChange={handleSalespersonchange} value={salesperson} required name="salesperson" id="salesperson" className='form-select'>
               <option value="">Choose a Salesperson</option>
               {salespeople?.map(sp => {
                return(
                    <option key={sp.employee_id} value={sp.employee_id}>
                        {sp.first_name} {sp.last_name}
                    </option>
                )
               })}
            </select>
            <table className="table table-striped">
            <thead>
                <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {filteredSales.map(sale => (
                    <tr key={sale.id}>
                      <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                      <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                      <td>{ sale.automobile.vin }</td>
                      <td>{ sale.price }</td>
                    </tr>
                ))}
            </tbody>
            </table>
        </div>
      );
}

export default SalesPersonHistory;
