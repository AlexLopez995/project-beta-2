import React, { useState, useEffect } from 'react';

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const fetchManufacturers = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  const handleDelete = async id => {
    const response = await fetch(`http://localhost:8100/api/manufacturers/${id}/`, {
      method: 'DELETE',
    });

    if (response.ok) {

      setManufacturers(manufacturers.filter(manufacturer => manufacturer.id !== id));
    }
  };

  return (
    <div className='container'>
      <h1>Manufacturers List</h1>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
              <td>
                <button onClick={() => handleDelete(manufacturer.id)} className='btn btn-danger'>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturersList;
