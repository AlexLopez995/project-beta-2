import React, { useState } from "react";

function ManufacturerForm() {
  const [name, setName] = useState("");
  const [successfulSubmit, setSuccessfulSubmit] = useState(false);

  let formClasses = "";
  let alertClasses = "alert alert-success d-none mb-3";
  let alertContainerClasses = "d-none";

  const handleSubmit = async event => {
    event.preventDefault();
    const data = { name: name };

    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerUrl, fetchConfig);

    if (response.ok) {
      setName("");
      setSuccessfulSubmit(true);
    }
  };

  const handleChange = event => {
    setName(event.target.value);
  };

  if (successfulSubmit) {
    formClasses = "d-none";
    alertClasses = "alert alert-success mb-3";
    alertContainerClasses = "";
  }

  return (
    <div className='row'>
      <div className='offset-3 col-6'>
        <div className='shadow p-4 mt-4'>
          <h1>Create a new Manufacturer</h1>
          <form onSubmit={handleSubmit} id='create-manufacturer-form' className={formClasses}>
            <div className='form-floating mb-3'>
              <input onChange={handleChange} value={name} placeholder='Manufacturer Name' required name='name' id='name' className='form-control' />
              <label htmlFor='name'>Name</label>
            </div>
            <button className='btn btn-primary'>Create</button>
          </form>
          <div className={alertContainerClasses}>
            <div className={alertClasses} id='success-message'>
              Great! New Manufacturer created successfully.
            </div>
            <button onClick={() => setSuccessfulSubmit(false)} className='btn btn-primary'>
              Create another Manufacturer
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
