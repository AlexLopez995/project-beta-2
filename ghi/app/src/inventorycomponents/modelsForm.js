import React, { useState, useEffect } from 'react';

function VehicleModelForm() {
  const [newModel, setNewModel] = useState({ name: '', picture_url: '', manufacturer_id: '' });
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {

    const fetchManufacturers = async () => {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
    };

    fetchManufacturers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    await fetch('http://localhost:8100/api/models/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newModel),
    });

    setNewModel({ name: '', picture_url: '', manufacturer_id: '' });
  };

  const handleChange = (event) => {
    setNewModel({ ...newModel, [event.target.name]: event.target.value });
  };

  return (
    <div className='container'>
      <h1>Add Vehicle Model</h1>
      <form onSubmit={handleSubmit}>
        <div className='form-group'>
          <label htmlFor='name'>Model Name:</label>
          <input type='text' id='name' name='name' className='form-control' value={newModel.name} onChange={handleChange} required />
        </div>
        <div className='form-group'>
          <label htmlFor='picture_url'>Picture URL:</label>
          <input type='url' id='picture_url' name='picture_url' className='form-control' value={newModel.picture_url} onChange={handleChange} required />
        </div>
        <div className='form-group'>
          <label htmlFor='manufacturer_id'>Manufacturer:</label>
          <select id='manufacturer_id' name='manufacturer_id' className='form-control' value={newModel.manufacturer_id} onChange={handleChange} required>
            <option value=''>Select a Manufacturer</option>
            {manufacturers.map((manufacturer) => (
              <option key={manufacturer.id} value={manufacturer.id}>
                {manufacturer.name}
              </option>
            ))}
          </select>
        </div>
        <button type='submit' className='btn btn-primary'>Submit</button>
      </form>
    </div>
  );
}

export default VehicleModelForm;
