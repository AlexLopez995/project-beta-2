from django.shortcuts import render
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_salesperson(request):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.all()
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonEncoder,
                content_type="application/json"
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"}, status=400, content_type="application/json")
    else:
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON"}, status=400, content_type="application/json")
        try:
            salesperson = Salesperson.objects.create(**content)
        except KeyError:
            return JsonResponse({"message": "Missing data"}, status=400, content_type="application/json")

        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
            content_type="application/json"
        )
@require_http_methods(["DELETE"])
def api_salesperson_details(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse({"deleted": True}, content_type="application/json")
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404, content_type="application/json")


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer":customer},
            encoder=CustomerEncoder,
            content_type="application/json"
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Customer cannot be created"},
                status=400,
                content_type="application/json"
            )
@require_http_methods(["DELETE"])
def api_customer_details(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse({"deleted": True}, content_type="application/json")
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404, content_type="application/json")

@require_http_methods(["GET", "POST"])
def api_list_sales(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            sale = Sale.objects.filter(automobile=automobile_vo_id)
        else:
            sale = Sale.objects.all()
        return JsonResponse({"sales": sale}, encoder=SaleEncoder)
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Automobile VIN invalid"}, status=400)
        try:
            salesperson = Salesperson.objects.get(id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson id invalid"}, status=400)
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer id invalid"}, status=400)
        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleEncoder, safe=False)

@require_http_methods(["DELETE"])
def api_sales_details(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse({"deleted": True}, content_type="application/json")
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404, content_type="application/json")
