# CarCar

Team:

* Person 1 - sales --alex
* Person 2 - service--anna

##  get start
1. folk---clone---
2. Build and run the project using Docker with these commands:

docker volume create beta-data
docker-compose build
docker-compose up

！install docker before use

After running these commands, make sure all of your Docker containers are running

View the project in the browser: http://localhost:3000/




## Design

Carcar has 3 microservices which interact with one another,

---inventory
---service
---sales

Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser


Carcar has 3 microservices which interact with one another,

---inventory
---service
---sales

we do service and sales first,
after finish, we do inventory together


## Service microservice
backend---APIs :

service microservice has 3 models in models.py. the AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the service microservice is constantly getting the updated data.

The logic of this part is that by creating new data for a technician, data for an appointment can be created. Both parts have their respective lists, constituting a partially nested relationship.
​

#technicians part :

list technicians (get) http://localhost:8080/api/technicians/
create new technicians (post) http://localhost:8080/api/technicians/
delete  technicians (delete) http://localhost:8080/api/technicians/id/

JSON body to send data :
create a technicians:
{
  "first_name": "Jane",
  "last_name": "Doe",
  "employee_id": "T1001"
}
return :
{
	"first_name": "Jane",
	"last_name": "Doe",
	"employee_id": "T1001",
	"id": 1
}

get the list:

{
	"technicians": [
		{
			"first_name": "Jane",
			"last_name": "Doe",
			"employee_id": "T1001",
			"id": 1
		}
	]
}

delete :

{
	"deleted": true
}

--------------------------------------------------------------------------------------------------------

#appointment part:

list appointment (get) http://localhost:8080/api/appointments/
create new appointment (post) http://localhost:8080/api/appointments/
delete appointment (delete) http://localhost:8080/api/appointments/7/
set status "canceled" (put) http://localhost:8080/api/appointments/4/cancel/
set status "finished" (put) http://localhost:8080/api/appointments/4/finish/

JSON code :

create appointmemt

{
  "date_time": "2023-12-18T10:00:00",
  "reason": "Routine maintenance",
  "status": "scheduled",
  "vin": "1HGCM82633A004352",
  "customer": "John Doe",
  "technician_id": 1
}

return :

{
	"date_time": "2023-12-18T10:00:00",
	"reason": "Routine maintenance",
	"status": "scheduled",
	"vin": "1HGCM82633A004352",
	"customer": "John Doe",
	"technician": {
		"first_name": "Jane",
		"last_name": "Doe",
		"employee_id": "T1001",
		"id": 1
	},
	"id": 1
}

list appointment return :

{
	"appointments": [
		{
			"date_time": "2023-12-18T10:00:00+00:00",
			"reason": "Routine maintenance",
			"status": "scheduled",
			"vin": "1HGCM82633A004352",
			"customer": "John Doe",
			"technician": {
				"first_name": "Jane",
				"last_name": "Doe",
				"employee_id": "T1001",
				"id": 1
			},
			"id": 1
		}
	]
}

delete:

{
	"deleted": true
}

set status return :

{
	"date_time": "2023-12-18T10:00:00+00:00",
	"reason": "Routine maintenance",
	"status": "canceled",
	"vin": "1HGCM82633A004352",
	"customer": "John Doe",
	"technician": {
		"first_name": "Jane",
		"last_name": "Doe",
		"employee_id": "T1001",
		"id": 1
	},
	"id": 1
}




or :

{
	"date_time": "2023-12-18T10:00:00+00:00",
	"reason": "Routine maintenance",
	"status": "finished",
	"vin": "1HGCM82633A004352",
	"customer": "John Doe",
	"technician": {
		"first_name": "Jane",
		"last_name": "Doe",
		"employee_id": "T1001",
		"id": 1
	},
	"id": 1
}
----------------------------------------------------------------------------------------------------

frondend - form and list display

On the front end, the form sends data to the back end, which receives it and then returns it to the list page.The aspect that needs explanation here is that the table will have some dropdown options, which are data sent from the form to the database and then called on the front end. Please ensure that the corresponding Form has already generated data, so that the dropdown options will have data available.

## Sales microservice

Explain your models and integration with the inventory micro service, here.
Sales Microservice models:

AutomobileVO- (vin, sold) One CharField and one BooleanField, respectively.
Salesperson- (first_name, last_name, employee_id) All CharField’s.
Customer- (first_name, last_name, address, phone_number) All CharField’s.
Sale- (automobile, salesperson, customer, price) Three ForeignKey’s and one CharField, respectively.

These models set the foundation for the necessary data relationships:
	The Automobile VO Model has two fields required for polling, the sold status and the VIN which is our unique identifier. The page listing sales can use the unique identifier to track the sold status of a vehicle.

	The Salesperson Model has three fields that help identify an employee. This model has a many to many relationship with the Sale Model because multiple salespeople can have various sales which have been recorded.

	The Customer Model has a one to many relationship with the Salesperson Model because one customer can have multiple purchases with various sales people.

	The Sale Model has four fields, two of which (salesperson and customer) identify who was involved in the sale, while automobile uses AutomobileVO to use the VIN as a unique identifier.

Front End API for Automobile Sales:
	List Salespeople- http://localhost:3000/salespeople
	Add a Salesperson- http://localhost:3000/salespeople/create/
	List Customers- http://localhost:3000/customer
	Add a Customer- http://localhost:3000/customer/create
	List Sales- http://localhost:3000/sale
	Add a Sale- http://localhost:3000/sale/new
	Salesperson History- http://localhost:3000/sale/history

Endpoints for sending and viewing Data using Insomnia:

Customer:
	List customers (GET method)- http://localhost:8090/api/customers/
	Create a customer (POST method)- http://localhost:8090/api/customers/
	Delete a customer (DELETE method)- http://localhost:8090/api/customers/id/

Send this JSON Body to create a customer:
{
	"first_name": “Tony”,
	"last_name": “Soprano”,
	"address" : “477 Not a Criminal street",
	"phone_number": "123-433-7777"
}
Return value for Creating a customer:
{
	"first_name": “Tony”,
	"last_name": “Soprano,
	"address": "477 Not a Criminal street",
	"phone_number": "123-433-7777",
	"id": 9
}
Return value for Listing all customers:
{
	"customer": [
		{
			"first_name": "doug",
			"last_name": "douglas",
			"address": "doug street",
			"phone_number": "123-456-7890",
			"id": 1
		},
{
			"first_name": "asdlfkjasdf",
			"last_name": "sdfsda",
			"address": "Whalessadfasfd$*#$% street",
			"phone_number": "123-433-7777",
			"id": 9
		}
	]
}
Return value for deleting a customer:
{
	"deleted": true
}

Salespeople:
List salespeople (GET method)- http://localhost:8090/api/salespeople/
Create a salesperson (POST method)- http://localhost:8090/api/salespeople/
Delete a salesperson (DELETE method)- http://localhost:8090/api/salespeople/id/

Send this JSON body to create a salesperson:
{
	"first_name": "Doodle",
	"last_name": "Bob",
	"employee_id" : "11234"
}
Return value for creating a salesperson:
{
	"first_name": "Doodle",
	"last_name": "Bob",
	"employee_id": "11234",
	"id": 5
}
Return value for Listing all salespeople:
{
	"salesperson": [
		{
			"first_name": "jeff",
			"last_name": "geoff",
			"employee_id": "12345",
			"id": 1
		},
		{
			"first_name": "dingus",
			"last_name": "malingus",
			"employee_id": "01234",
			"id": 2
		},
		{
			"first_name": "sdfsadf",
			"last_name": "sdfasdf",
			"employee_id": "sdfasdfsdf",
			"id": 3
		},
		{
			"first_name": "Buzz",
			"last_name": "Lightyear",
			"employee_id": "asdljsd",
			"id": 4
		},
		{
			"first_name": "Doodle",
			"last_name": "Bob",
			"employee_id": "11234",
			"id": 5
		}
	]
}
Return value for deleting a salesperson:
{
	"deleted": true
}

Sale:
List sale (GET method)- http://localhost:8090/api/sales/
Create a sale (POST method)- http://localhost:8090/api/sales/
Delete a sale (DELETE method)- http://localhost:8090/api/sales/id/

Send this JSON body to create a sale:
{
  "automobile": "1C3CC5FB2AN120174",
  "salesperson": "1",
  "customer": 1,
	"price": 12020
}
Return value for creating a sale:
{
	"automobile": {
		"vin": "1C3CC5FB2AN120174",
		"sold": false
	},
	"salesperson": {
		"first_name": "jeff",
		"last_name": "geoff",
		"employee_id": "12345",
		"id": 1
	},
	"customer": {
		"first_name": "doug",
		"last_name": "douglas",
		"address": "doug street",
		"phone_number": "123-456-7890",
		"id": 1
	},
	"price": 12020,
	"id": 7
}
Return value for listing sales:
{
	"sales": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"sold": true
			},
			"salesperson": {
				"first_name": "jeff",
				"last_name": "geoff",
				"employee_id": "12345",
				"id": 1
			},
			"customer": {
				"first_name": "doug",
				"last_name": "douglas",
				"address": "doug street",
				"phone_number": "123-456-7890",
				"id": 1
			},
			"price": "12020",
			"id": 7
		},
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"sold": true
			},
			"salesperson": {
				"first_name": "jeff",
				"last_name": "geoff",
				"employee_id": "12345",
				"id": 1
			},
			"customer": {
				"first_name": "doug",
				"last_name": "douglas",
				"address": "doug street",
				"phone_number": "123-456-7890",
				"id": 1
			},
			"price": "sdfsdfsdf",
			"id": 8
		},
		{
			"automobile": {
				"vin": "1C3CB6FB2AN120000",
				"sold": true
			},
			"salesperson": {
				"first_name": "Buzz",
				"last_name": "Lightyear",
				"employee_id": "asdljsd",
				"id": 4
			},
			"customer": {
				"first_name": "Tony",
				"last_name": "Soprano",
				"address": "New Joysee",
				"phone_number": "8888888888",
				"id": 4
			},
			"price": "1700",
			"id": 9
		}
	]
}
Return value for deleting a sale:
Return value for deleting a salesperson:
{
	"deleted": true
}


Diagram:
![Alt text](Screenshot_2023-12-22_at_11.02.53_AM.png)



### Inventory API

In the inventory section, the logic is as follows: First, a form for 'manufacture' is required to obtain the name of a new manufacturer, followed by a page displaying a list of manufacturers. Second, for models, there is a form to add a new model, which includes the already established manufacturers. This is followed by a page displaying a list of new models. Third, there is a form to add a new automobile, which includes the already established models, leading to a list of automobiles that contains all the information from the form. Therefore, this section exhibits a partially nested relationship.
