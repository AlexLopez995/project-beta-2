from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Technician,Appointment,AutomobileVO
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods




class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
        ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
        ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id"
        ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
            )

    content = json.loads(request.body)
    technician = Technician.objects.create(**content)
    return JsonResponse(
        technician,
        encoder=TechnicianEncoder,
        safe=False
        )


@require_http_methods(["DELETE"])
def api_technician_detail(request, pk):
    technician = get_object_or_404(Technician, id=pk)
    technician.delete()
    return JsonResponse(
        {"deleted": True}
        )


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder
            )

    content = json.loads(request.body)
    technician = get_object_or_404(
        Technician,
        id=content.pop(
            'technician_id',
            None)
        )
    appointment = Appointment.objects.create(technician=technician, **content)
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
        )


@require_http_methods(["DELETE"])
def api_appointment_detail(request, pk):
    Appointment.objects.filter(id=pk).delete()
    return JsonResponse(
        {"deleted": True}
        )


@require_http_methods(["PUT"])
def api_set_appointment_status(request, pk, status):
    appointment = get_object_or_404(Appointment, id=pk)
    appointment.status = status
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False
        )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    return api_set_appointment_status(request, pk, "canceled")


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    return api_set_appointment_status(request, pk, "finished")
