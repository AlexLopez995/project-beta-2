from django.urls import path
from . import views

urlpatterns = [
    path('technicians/',views.api_technicians, name='technicians_list'),
    path('technicians/<int:pk>/', views.api_technician_detail, name='technicians_detail'),
    path('appointments/',views.api_appointments,name='appointment_list'),
    path('appointments/<int:pk>/', views.api_appointment_detail, name='appointment_detail'),
    path('appointments/<int:pk>/cancel/', views.api_cancel_appointment, name='cancel_appointment'),
    path('appointments/<int:pk>/finish/', views.api_finish_appointment, name='finish_appointment'),


]
